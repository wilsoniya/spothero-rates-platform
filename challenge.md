# **Sample Problem**

--------------------

#### Build an API that allows a user to enter a date time range and get back
the rate at which they would be charged to park for that time span

* The application publishes an API that computes a price for an input datetime
  range
* The user specifies input date/times as ISO-8601 with timezones
* Rates are specified by a JSON file
  * A rate is comprised of a price, time range the rate is valid, and days of
    the week the rate applies to
* User input can span more than one day, but the API shouldn't return a valid
  rate
* User input can span multiple rates, but the API shouldn't return a valid rate
* Rates will not span multiple days
* The rate information can be updated by submitting a modified rates JSON and
  can be stored in memory
  * how will the newly supplied rates be merged into existing rates?

## Requirements

* Use Java, Kotlin, Python, or Go to complete this
* It should support JSON over HTTP
* API endpoints should be documented
* Tests need to be in place

## Extra Credit

* Include a Swagger Spec
* Include a Dockerfile
* Metrics for endpoint(s) captured and available to be queried via an endpoint
  (e.g. average response time)

## Submitting
* Publish a repository on your preferred hosting platform. Include a link to
  the repository in your submission
* Include any instructions on how to build, run, and test your application

## Sample JSON for testing

```json
{
    "rates": [
        {
            "days": "mon,tues,thurs",
            "times": "0900-2100",
            "tz": "America/Chicago",
            "price": 1500
        },
        {
            "days": "fri,sat,sun",
            "times": "0900-2100",
            "tz": "America/Chicago",
            "price": 2000
        },
        {
            "days": "wed",
            "times": "0600-1800",
            "tz": "America/Chicago",
            "price": 1750
        },
        {
            "days": "mon,wed,sat",
            "times": "0100-0500",
            "tz": "America/Chicago",
            "price": 1000
        },
        {
            "days": "sun,tues",
            "times": "0100-0700",
            "tz": "America/Chicago",
            "price": 925
        }
    ]
}
```

## Sample result
Datetime ranges should be specified in ISO-8601 format. A rate must completely
encapsulate a datetime range for it to be available.

Rates will never overlap

* `2015-07-01T07:00:00-05:00` to `2015-07-01T12:00:00-05:00` should yield
  `1750`
* `2015-07-04T15:00:00+00:00` to `2015-07-04T20:00:00+00:00` should yield
  `2000`
* `2015-07-04T07:00:00+05:00` to `2015-07-04T20:00:00+05:00` should yield
  `unavailable`
