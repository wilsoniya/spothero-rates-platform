# Rates Platform

## TL;DR

```bash
go get gitlab.com/wilsoniya/spothero-rates-platform/rates-service
$GOPATH/bin/rates-service
```

## Contents

* [Rates service](#rates-service)
  * [Building](#building)
    * [Native build](#native-build)
    * [Docker build](#docker-build)
  * [Running](#running)
  * [Testing](#testing)
  * [A word on datetime range in/exclusivity](#a-word-on-datetime-range-inexclusivity)
* [HTTP API](#http-api)
  * [Routes](#routes)
  * [Generating swagger docs and bindings](#generating-swagger-docs-and-bindings)
* [Deployment](#deployment)
  * [Docker](#docker)
  * [Docker Compose](#docker-compose)
  * [Kubernetes](#kubernetes)
  * [Monitoring](#monitoring)
* [Further improvements](#further-improvements)


## Rates service

**rates-service** is a Go service presenting an HTTP API for querying and
modifying parking rates for specific days of the week and times.

This project was implemented as a response to a [code
challenge](challenge.md) from SpotHero.

### Building

#### Native build

Prerequisites:

* properly configured `$GOPATH`, with `$GOPATH/bin` in `$PATH`
* `go1.11` (tested with `go1.11.5 linux/amd64`)

```bash
go get gitlab.com/wilsoniya/spothero-rates-platform/rates-service
cd $GOPATH/src/gitlab.com/wilsoniya/spothero-rates-platform/rates-service

# on the first build
make bootstrap

# for subsequent builds
make
```

#### Docker build

For convenience, a dockerized build is included for compilation without having
to install the Go tool chain.

After cloning this repository, do:

```bash
cd rates-service
make docker-build
```

### Running

To run **rates-service**:

```bash
cd rates-service

make run

# or

make
./rates-service
```

**rates-service** listens on TCP `0.0.0.0:8080`.

### Testing

```bash
cd rates-service

make test
make lint
```

A very limited integration smoke test is also included:

```bash
# in the repository root directory:
./smoke_test.sh
Using API URL: http://localhost:8080
Smoke test passed.

# increased verbosity
VERBOSE=1 ./smoke_test.sh
```

### A word on datetime range in/exclusivity

When determining whether a query datetime range fits within a reference range,
**rates-service** treats the start of the reference range as inclusive and the
end as exclusive. This implies it's valid for a query time to *begin*
concurrently with the start of the reference range, but the query must
*end* before the end of the datetime range.

E.g.:

* :x: `reference: 0900-1700` - `query: 0900-1700`
* :white_check_mark: `reference: 0900-1700` - `query: 0900-1659`

The motivation for this is to avoid overlaps in the situation in which a
reference range immediately followed by another range.


* `reference A: 0900-1000` == `09:00:00.000-09:59:59.999`
* `reference B: 1000-1100` == `10:00:00.000-10:59:59.999`

## HTTP API

### Routes

* `GET` `/v1/rate/{startDatetimeInclusive}/{endDatetimeExclusive}` - queries
  rates within given datetime bounds
* `POST` `/v1/rate` - sets the collection of rates used to answer rate queries
* `GET` `/v1/stats` - gets runtime statistics about API routes

### Generating swagger docs and bindings

The public HTTP API exposed by **rates-service** is defined by Swagger.

* [API Definition](rates-service/swagger.yaml).

**Note**: `swagger-codegen` is used to validate the API specification and
generate Client API Definition docs, but the resulting go code isn't used in
**rates-service**.

Though they aren't used by **rates-service**, the swagger client and server
bindings can be generated for fun. Building docs and bindings can be used to
validate the format of `swagger.yaml`.

```bash
cd rates-service

# kitchen sink: build docs, client, and server bindings
make swagger-codegen

# build and view docs
make view-docs
```

## Deployment

Several deployment mechanisms have been formulated for use with
**rates-service**.

### Docker

**rates-service** can be run in a standalone docker container:

```bash
cd deployment
make docker-run

# rates-service runs in docker connected to tty.
# from another terminal window rates-service is available at http://localhost:8080
```

### Docker Compose

**rates-service** as well as some peripheral infrastructure can be run using
docker-compose:

```bash
cd deployment
make docker-compose-up

# rates-service is available at http://localhost:8080
# Grafana is available at http://localhost:9080 (user: admin, pass: admin)
# Graphite is available at http://localhost:9081 (user: root, pass: root)

make docker-compose-down
```

### Kubernetes

**rates-service** as well as peripheral infrastructure can be run on
Kubernetes using minikube.

**Prerequisites**

* minikube
* kubectl
* docker

```bash
cd deployment

# go from zero to smoke tests in k8s; this might take a few minutes.
# Depending on the speed of your machine, the smoke test may try to execute
# before rates-service is fully deployed. If they fail the first time, give
# them another try.
make k8s-smoketest

# open the stats route in a web browser (refresh and watch what happens)
make k8s-open-stats

# open Graphite and Grafana in a web browser
# Grafana user: admin, pass: admin
# Graphite user: root, pass: root
make k8s-open-monitoring

# open k8s dashboard in a web browser
make k8s-dashboard

# tear it all down
make minikube-delete
```

### Monitoring

**rates-service** is instrumented with statsd to emit metrics describing
operational and performance characteristics. In a production setting, this
would be used to established a historical basis for performance and as input to
an alerting system. **rates-service** will happily run without an available
statsd daemon, but if one is reachable in the execution environment at
`upd://statsd:8125`, it will pick up telemetry.

Docker-compose and Kubernetes deployments both include a monitoring stack with
statsd, Graphite, and Grafana. Grafana must be manually configured to draw data
from Graphite, as there was no simple way to preconfigure this.  The monitoring
container does not mount any volumes, so all storage is ephemeral.

## Further improvements

Though **rates-service** is a well-tested, highly visible microservice with a
variety of deployment options, there's always room for improvement. Here are
some of the things I didn't get to tackle during the code challenge (in no
particular order):

* **Improvements to rate query algorithm**: The current algorithm has a runtime
  linear in the number of stored rates. I judged this acceptable given the
  scope and constraints of the code challenge but a productionized
  implementation run at scale would require a sub-linear runtime. Constraining
  the pool of rates to a single time zone would allow for the creation of a
  sorted index on start and end times, enabling logarithmic search. This
  adjustment of constraints seems realistic given that locations for parking
  don't generally span time zones (or it limits the variation to two time
  zones). Weekday bucketing and bounded date ranges of rate applicability could
  reduce the search space (albeit without further reducing algorithmic
  runtime).
* **Persistent storage**: Rates data is stored in-memory which is great for
  speed and simplicity, but in its present form, **rates-service** could not be
  scaled out because rate updates would immediately lead to complete
  inconsistency between service processes. Put another way, persistent storage
  would make **rates-service** stateless, which is desirable.
* **Test coverage reporting**
* **Performance testing**: In a production setting, performance testing would
  be essential to establish and maintain service level objectives.
* **Substantial integration testing**: The simple
  [`smoke_test.sh`](https://gitlab.com/wilsoniya/spothero-rates-platform/blob/master/smoke_test.sh)
  is handy to see if the API is up and healthy, but it's not exhaustive and
  wouldn't be reliable to detect subtle regressions.
* **Opentracing integration**
* **Persistent log capture using ELK**
* **Config handling**: in its current state **rates-service** is hard-coded to
  listen on `0.0.0.0:8080`. A productionized version would be configurable in
  this and other ways.
* **Integration of an alerting system with stats collection**: e.g.
  [Cabot](https://cabotapp.com/)
* **CI, release automation, versioning, deployment**
