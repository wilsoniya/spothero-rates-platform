#!/usr/bin/env sh

# builds rates-service docker image on minikube vm

image_name=$1

eval $(minikube docker-env)
docker build -t ${image_name} .
