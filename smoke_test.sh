#!/usr/bin/env bash

##
# A quick and dirty integration smoke test suite for the rates-service HTTP API
#

api_url=${1:-http://localhost:8080}

echo "Using API URL: ${api_url}"

if ! hash curl; then
    >&2 echo "ERROR: please install curl"
    exit 127
fi

if [[ -n ${VERBOSE} ]]; then
    v="-v"
fi

function fail() {
    msg=$1
    >&2 echo "FAIL: ${msg}"
    kill ${PID}

    exit 1
}

cd rates-service
make &> /dev/null

./rates-service &> /dev/null &
PID=$!

sleep "0.3"

# no rates loaded
expected=unavailable
rate=$(curl $v -s localhost:8080/v1/rate/2015-07-04T15:00:00+00:00/2015-07-04T20:00:00+00:00)
[[ ${rate} == ${expected} ]] || fail "when querying rate: expected ${expected}, got \"${rate}\""

# set rates
expected=200
code=$(curl $v -s -o /dev/null -w "%{http_code}" --data @../rates.json ${api_url}/v1/rate)
[[ ${code} == ${expected} ]] || fail "when uploading rates: expected ${expected}, got \"${code}\""

# query rate success
expected=2000
rate=$(curl $v -s localhost:8080/v1/rate/2015-07-04T15:00:00+00:00/2015-07-04T20:00:00+00:00)
[[ ${rate} == ${expected} ]] || fail "when querying rate: expected ${expected}, got \"${rate}\""

# no rate found
expected=404
code=$(curl $v -s -o /dev/null -w "%{http_code}" ${api_url}/v1/rate/2015-07-04T07:00:00+05:00/2015-07-04T20:00:00+05:00)
[[ ${code} == ${expected} ]] || fail "when querying rate: expected code ${expected}, got \"${code}\""

# no rate found
expected=unavailable
rate=$(curl $v -s ${api_url}/v1/rate/2015-07-04T07:00:00+05:00/2015-07-04T20:00:00+05:00)
[[ ${rate} == ${expected} ]] || fail "when querying rate: expected ${expected}, got \"${rate}\""

echo "Smoke test passed."

kill ${PID}
