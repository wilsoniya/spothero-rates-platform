package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strconv"
	"time"
)

var rfc3339Fmt = `(\d{4}-(?:0[1-9]|1[0-2])-(?:0[1-9]|[12][0-9]|3[01])T(?:[01][0-9]|2[0-3]):[0-5][0-9]:(?:[0-5][0-9]|60)(?:\.[0-9]+)?(?:Z|(?:\+|-)(?:[01][0-9]|2[0-3]):(?:[0-5][0-9])))`

// routeParams encapsulates extra parameters sent to routed handler functions
type routeParams struct {
	routeName   string
	pathMatches []string
}

// handlerFunc is an extension of http.HandlerFunc augmented with extra parameters
type handlerFunc = func(writer http.ResponseWriter, request *http.Request, params routeParams)

// routes expresses a mapping from HTTP path patterns to handler functions
var routes = []struct {
	Name        string
	Method      string
	Pattern     *regexp.Regexp
	HandlerFunc handlerFunc
}{
	{
		"GetRate",
		http.MethodGet,
		regexp.MustCompile(fmt.Sprintf(`^/v1/rate/%s/%s$`, rfc3339Fmt, rfc3339Fmt)),
		handleGetRate,
	},
	{
		"SetRates",
		http.MethodPost,
		regexp.MustCompile(`^/v1/rate$`),
		handleSetRates,
	},
	{
		"GetStats",
		http.MethodGet,
		regexp.MustCompile(`^/v1/stats$`),
		handleGetStats,
	},
}

// routeHandler is a generic regexp-based HTTP router
var routeHandler http.HandlerFunc = func(writer http.ResponseWriter, request *http.Request) {
	Statsd.Increment("server.request.received")

	for _, route := range routes {
		if request.Method != route.Method {
			Logger.Debugw(
				"Method mismatch",
				"path", request.URL.Path,
				"pattern", route.Pattern,
				"requestMethod", request.Method,
				"routeMethod", route.Method,
			)
			continue
		}

		matches := route.Pattern.FindStringSubmatch(request.URL.Path)
		if matches == nil {
			Logger.Debugw(
				"Match failure",
				"path", request.URL.Path,
				"pattern", route.Pattern,
			)
			continue
		}

		Logger.Infow(
			"Routed request",
			"path", request.URL.Path,
			"method", request.Method,
		)
		params := routeParams{route.Name, matches}

		defer Statsd.NewTiming().Send(fmt.Sprintf("route.%s.elapsed", route.Name))
		defer Stats.TimeRoute(route.Name)()

		route.HandlerFunc(writer, request, params)

		Statsd.Increment(fmt.Sprintf("route.%s.requested", route.Name))
		Statsd.Increment("server.request.routed")
		return
	}

	Logger.Infow(
		"Unroutable request",
		"path", request.URL.Path,
		"method", request.Method,
	)
	writer.WriteHeader(http.StatusNotFound)
	writer.Write([]byte("404 Not Found"))
	Statsd.Increment("server.request.unroutable")
}

// handleGetRate handles rate queries over a given datetime range
func handleGetRate(writer http.ResponseWriter, request *http.Request, params routeParams) {
	if params.pathMatches == nil {
		// this should never happen because path regexp will have already captured params
		Logger.Errorw(
			"nil pathMatches passed",
			"routeName", params.routeName,
			"path", request.URL.Path,
		)
		Statsd.Increment("route.get_rate.nil_path_matches")
		writer.WriteHeader(http.StatusNotFound)
		writer.Write([]byte("404 Not Found"))
		return
	}

	begin, beginErr := time.Parse(time.RFC3339, params.pathMatches[1])
	end, endErr := time.Parse(time.RFC3339, params.pathMatches[2])

	if beginErr != nil || endErr != nil {
		// this shouldn't happen because route already matched regexp
		Logger.Errorw(
			"Failed to parse beginning and/or end rate datetimes",
			"beginErr", beginErr,
			"endErr", endErr,
			"begin", params.pathMatches[1],
			"end", params.pathMatches[2],
		)
		Statsd.Increment("route.get_rate.date_range_parse_failed")
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("Failed to parse time range"))
		return
	}

	store := GetStore()
	rates := store.GetRates(begin, end)

	if len(rates) == 0 {
		Logger.Infow(
			"No rates found for datetime range",
			"begin", begin,
			"end", end,
		)
		Statsd.Increment("route.get_rate.no_rates_found")
		writer.Header().Set("Content-Type", "text/plain; charset=utf-8")
		writer.WriteHeader(http.StatusNotFound)
		writer.Write([]byte("unavailable"))
		return
	} else if len(rates) > 1 {
		// warning because code challenge constraints specified:
		// "Rates will never overlap"
		Logger.Warnw(
			"Multiple rates found for datetime range",
			"begin", begin,
			"end", end,
		)
		Statsd.Increment("route.get_rate.multiple_rates_found")
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("Internal server error"))
		return
	} else {
		price := rates[0].Price
		Logger.Infow(
			"Found rate for datetime range",
			"rate", price,
		)
		Statsd.Increment("route.get_rate.success")
		writer.WriteHeader(http.StatusOK)
		writer.Write([]byte(fmt.Sprintf("%d", price)))
		return
	}
}

// handleSetRates handles setting the contents of the rates store
func handleSetRates(writer http.ResponseWriter, request *http.Request, params routeParams) {
	body, err := ioutil.ReadAll(request.Body)

	if err != nil {
		Logger.Errorw(
			"Error reading request body",
			"error", err,
		)
		Statsd.Increment("route.set_rates.request_body_read_error")
		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("Internal server error"))
		return
	}

	var apiRates APIRates
	err = json.Unmarshal(body, &apiRates)

	if err != nil {
		Logger.Warnw(
			"Unable to deserialize rates from request body",
			"error", err,
			"body", string(body),
		)
		Statsd.Increment("route.set_rates.rates_deserialization_failure")
		writer.WriteHeader(http.StatusBadRequest)
		writer.Write([]byte("Invalid input"))
		return
	}

	var rates []Rate
	for _, apiRate := range apiRates.APIRates {
		rate, err := apiRate.AsRate()

		if err != nil {
			Logger.Warnw(
				"Unable to validate rate",
				"error", err,
				"rate", apiRate,
			)
			Statsd.Increment("route.set_rates.rate_validation_failure")
			writer.WriteHeader(http.StatusBadRequest)
			writer.Write([]byte("Invalid input"))
			return
		}

		rates = append(rates, *rate)
	}

	store := GetStore()
	store.SetRates(rates)

	Logger.Infow("Successfully stored rates")
	Statsd.Increment("route.set_rates.success")

	writer.WriteHeader(http.StatusOK)
	writer.Write([]byte("Success"))
}

// handleGetStats retrieves some interesting stats
func handleGetStats(writer http.ResponseWriter, request *http.Request, params routeParams) {
	summary := Stats.GetSummary()
	summaryBytes, err := json.Marshal(summary)

	if err != nil {
		Logger.Errorw(
			"Error serializing Summary",
			"error", err,
			"summary", summary,
		)
		Statsd.Increment("route.get_stats.stats_serialization_failure")

		writer.WriteHeader(http.StatusInternalServerError)
		writer.Write([]byte("Internal server error"))
		return
	}

	Statsd.Increment("route.get_stats.success")

	writer.Header().Set("Content-Type", "application/json; charset=utf-8")
	writer.Header().Set("Content-Length", strconv.Itoa(len(summaryBytes)))
	writer.Write(summaryBytes)
}

// Run starts the rates API server.
func Run() {
	listenInterface := ":8080"

	server := &http.Server{
		Addr:           listenInterface,
		Handler:        routeHandler,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	Logger.Infof("Listening on: %s", listenInterface)
	Logger.Fatal(server.ListenAndServe())
}
