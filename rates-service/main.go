package main

import (
	"log"

	"go.uber.org/zap"
	statsd "gopkg.in/alexcesaro/statsd.v2"
)

// statsdAddr is the hostname:port address at which statsd can be reached.
// The statsd library can tolerate an unreachable statsd daemon.
const statsdAddr string = "statsd:8125"

// Logger is a global reference to the logger
var Logger *zap.SugaredLogger

// Statsd is a global reference to the monitoring system
var Statsd *statsd.Client

// Stats is a global reference to the StatsStore
var Stats StatsStore

func setupLogging() func() {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Fatalf("FATAL: could not initialize logger: %v", err)
	}
	Logger = logger.Sugar()
	Logger.Infof("Rates service logging online")

	return func() {
		logger.Sync() // flushes buffer, if any
	}
}

func setupMonitoring() func() {
	var err error

	Statsd, err = statsd.New(
		statsd.Address(statsdAddr),
		statsd.Prefix("rates"),
	)
	if err != nil {
		// If nothing is listening on the target port, an error is returned and
		// the returned client does nothing but is still usable. So we can
		// just log the error and go on.
		Logger.Warnw(
			"Error establishing statsd connection",
			"error", err,
			"statsdAddr", statsdAddr,
		)
	} else {
		Logger.Infow("Monitoring online")
	}

	return func() {
		Statsd.Close()
	}
}

func setupStatsCollection() func() {
	Stats = NewStatsStore()

	return func() {
		Stats.Close()
	}
}

func main() {
	defer setupLogging()()
	defer setupMonitoring()()
	defer setupStatsCollection()()

	Statsd.Increment("application.startup")
	Run()
	Statsd.Increment("application.shutdown")
}
