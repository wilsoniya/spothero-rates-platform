# Special acknowledgement to SpotHero for some golang Makefile inspiration:
# https://github.com/spothero/periodic/blob/master/Makefile

GOSRC = $(shell find . -path ./vendor -prune -o -name '*.go' -print)
UID = $(shell id -u)
GID = $(shell id -g)
GO_PREREQUISITES = \
	github.com/golang/dep/cmd/dep \
	github.com/alecthomas/gometalinter
LINTERS = deadcode gofmt golint ineffassign misspell staticcheck vet
METALINT = gometalinter --tests --disable-all --vendor --deadline=5m \
		   -e "swagger" ./...
OS = $(shell uname)

ifeq (${OS},Linux)
OPEN = xdg-open
else
OPEN = open
endif

.PHONY: all bootstrap clean docker-build lint run \
	swagger-client-codegen swagger-docs swagger-codegen \
	swagger-server-codegen test view-docs

all: rates-service

rates-service: vendor ${GOSRC}
	go build

run: rates-service
	./rates-service

docker-build:
	docker run \
		--rm \
		-v ${CURDIR}:/go/src/rates-service \
		-w /go/src/rates-service \
		golang:1.11.5 \
		make bootstrap UID=${UID} GID=${GID}

${GO_PREREQUISITES}:
	go get -u $@

test:
	go test

bootstrap: ${GO_PREREQUISITES}
	gometalinter --install
	$(MAKE)
	# chown shenanigans are necessary to adjust ownership after docker builds
	-chown -R ${UID}:${GID} rates-service vendor

vendor:
	dep ensure -v -vendor-only

clean:
	rm -rf rates-service
	rm -rf vendor

lint:
	${METALINT} $(addprefix --enable=,${LINTERS})

${LINTERS}:
	${METALINT} --enable=$@

swagger-codegen: swagger-server-codegen swagger-docs swagger-client-codegen

swagger-server-codegen:
	docker run \
		--rm \
		-v ${CURDIR}:/local \
		swaggerapi/swagger-codegen-cli \
			generate \
			-i /local/swagger.yaml \
			-l go-server \
			-o /local/out/go
	rm -rf ./swagger/server
	mkdir -p ./swagger
	mv ./out/go/go ./swagger/server
	rm -rf ./out

swagger-client-codegen:
	docker run \
		--rm \
		-v ${CURDIR}:/local \
		swaggerapi/swagger-codegen-cli \
			generate \
			-i /local/swagger.yaml \
			-l go \
			-o /local/out/go
	rm -rf ./swagger/client
	mkdir -p ./swagger
	mv ./out/go ./swagger/client
	rm -rf ./out

swagger-docs: ./swagger/docs/index.html
./swagger/docs/index.html: swagger.yaml
	docker run \
		--rm \
		-v ${CURDIR}:/local \
		swaggerapi/swagger-codegen-cli \
			generate \
			-i /local/swagger.yaml \
			-l html \
			-o /local/out/go
	rm -rf ./swagger/docs
	mkdir -p ./swagger
	mv ./out/go ./swagger/docs
	rm -rf ./out

view-docs: ./swagger/docs/index.html
	${OPEN} ./swagger/docs/index.html
