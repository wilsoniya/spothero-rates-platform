package main

import (
	"log"
	"os"
	"testing"

	"go.uber.org/zap"
	statsd "gopkg.in/alexcesaro/statsd.v2"
)

func TestMain(m *testing.M) {
	// setup zap logger to avoid nil pointer deref during testing
	_logger, err := zap.NewDevelopment()

	if err != nil {
		log.Fatalf("FATAL: could not initialize logger: %v", err)
	}

	defer _logger.Sync() // flushes buffer, if any
	Logger = _logger.Sugar()
	Statsd, _ = statsd.New()
	Stats = NewStatsStore()

	code := m.Run()
	os.Exit(code)
}
