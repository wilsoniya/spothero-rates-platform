# Summary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RouteSummaries** | [**[]RouteSummary**](RouteSummary.md) | performance of each route that has been invoked. | [optional] [default to null]
**UptimeSecs** | **int32** | total time in seconds since the rates-service process was started | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


