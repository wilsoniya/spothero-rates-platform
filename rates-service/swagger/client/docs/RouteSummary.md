# RouteSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**RouteName** | **string** | A unique name for the route | [optional] [default to null]
**MeanElapsedMs** | **float32** | Average time elapsed while processing the route in milliseconds | [optional] [default to null]
**HitCount** | **int64** | Total number of invocations of the route | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


