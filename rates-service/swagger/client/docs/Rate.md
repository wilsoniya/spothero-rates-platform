# Rate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Days** | **string** | comma-delimited list of days of the week (in [mon|tues|wed|thurs|fri|sat|sun] on which the rate is applicable. | [optional] [default to null]
**Times** | **string** | time range of rate validity of form hhmm-hhmm. first time must occur strictly before the second. | [optional] [default to null]
**Tz** | **string** | TZ database name of timzone. See: https://en.wikipedia.org/wiki/List_of_tz_database_time_zones | [optional] [default to null]
**Price** | **int32** | rate price in USD cents | [optional] [default to null]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


