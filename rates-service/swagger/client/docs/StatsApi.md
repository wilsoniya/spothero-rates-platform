# \StatsApi

All URIs are relative to *http://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetStats**](StatsApi.md#GetStats) | **Get** /stats | Get statistics for all API routes


# **GetStats**
> Summary GetStats(ctx, )
Get statistics for all API routes

### Required Parameters
This endpoint does not need any parameter.

### Return type

[**Summary**](Summary.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

