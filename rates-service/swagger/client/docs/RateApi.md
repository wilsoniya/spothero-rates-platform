# \RateApi

All URIs are relative to *http://localhost/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**GetRate**](RateApi.md#GetRate) | **Get** /rate/{startDatetime}/{endDatetime} | Get a rate for a given span of time
[**SetRates**](RateApi.md#SetRates) | **Post** /rate | Set rates managed by the service


# **GetRate**
> int32 GetRate(ctx, startDatetime, endDatetime)
Get a rate for a given span of time

### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **startDatetime** | **time.Time**| Start datetime (inclusive) of the span of time | 
  **endDatetime** | **time.Time**| Start datetime (exclusive) of the span of time | 

### Return type

**int32**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **SetRates**
> SetRates(ctx, rates)
Set rates managed by the service



### Required Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
  **rates** | [**Rates**](Rates.md)| Rates object to be managed by service | 

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

