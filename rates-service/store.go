package main

import (
	"sync"
	"time"
)

// store is a global reference to the RatesStore. Use GetStore() to interact
// with the RatesStore.
var store RatesStore

// RatesStore provides storage of rates for parking corresponding to particular
// times.
type RatesStore interface {
	// SetRates replaces any currently stored rates with the provided rates
	SetRates(rates []Rate)

	// GetRates queries the store for rates valid in the range [beginInclusive, endExclusive)
	GetRates(beginInclusive time.Time, endExclusive time.Time) []Rate
}

// inMemoryRatesStore is a RatesStore which stores rates in volatile memory.
type inMemoryRatesStore struct {
	rates []Rate

	// ratesMutex protects rates from race conditions while enabling parallel reads
	ratesMutex sync.RWMutex
}

// GetStore returns a handle to the global RatesStore
func GetStore() RatesStore {
	if store == nil {
		store = &inMemoryRatesStore{}
	}

	return store
}

func (s *inMemoryRatesStore) SetRates(rates []Rate) {
	defer Statsd.NewTiming().Send("in_memory_rate_store.set_rates.elasped")
	Statsd.Increment("in_memory_rate_store.set_rates.invoked")
	Statsd.Histogram("in_memory_rate_store.set_rates.num_rates", len(rates))

	s.ratesMutex.Lock()
	defer s.ratesMutex.Unlock()

	s.rates = rates
}

func (s *inMemoryRatesStore) GetRates(beginInclusive time.Time, endExclusive time.Time) []Rate {
	defer Statsd.NewTiming().Send("in_memory_rate_store.get_rates.elasped")
	Statsd.Increment("in_memory_rate_store.get_rates.invoked")

	s.ratesMutex.RLock()
	defer s.ratesMutex.RUnlock()

	matchingRates := make([]Rate, 0, len(s.rates))

	for _, rate := range s.rates {
		if rate.SpansTimes(beginInclusive, endExclusive) {
			matchingRates = append(matchingRates, rate)
		}
	}

	Statsd.Histogram("in_memory_rate_store.get_rates.num_matching_rates", len(matchingRates))

	return matchingRates
}
