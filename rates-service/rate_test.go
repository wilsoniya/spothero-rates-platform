package main

import (
	"reflect"
	"testing"
	"time"
)

var chicago, _ = time.LoadLocation("America/Chicago")

// TestTest is just a few assertions that were helpful during development
func TestTest(t *testing.T) {
	rfc3339 := "2015-07-01T07:00:00-05:00"
	rfc3339_2 := "2015-07-04T15:00:00+00:00"

	_time, err := time.Parse(time.RFC3339, rfc3339)
	if err != nil {
		t.Errorf("Error parsing datetime: %v", err)
	}
	t.Logf("original: %v, modified: %v", _time, _time.UTC())

	_time, err = time.Parse(time.RFC3339, rfc3339_2)
	if err != nil {
		t.Errorf("Error parsing datetime: %v", err)
	}
	t.Logf("original: %v, modified: %v", _time, _time.UTC())

	if chicago.String() != "America/Chicago" {
		t.Errorf("time.LoadLocation() and Location.String() are not inverse functions")
	}
}

var validationTests = []struct {
	in  APIRate
	out *Rate
	err error
}{
	// successes
	{
		APIRate{"mon,tues,wed,thurs,fri,sat,sun", "0900-2100", "America/Chicago", 1500},
		&Rate{
			WeekdaySet{
				time.Monday:    struct{}{},
				time.Tuesday:   struct{}{},
				time.Wednesday: struct{}{},
				time.Thursday:  struct{}{},
				time.Friday:    struct{}{},
				time.Saturday:  struct{}{},
				time.Sunday:    struct{}{},
			},
			900,
			2100,
			*chicago,
			1500,
		},
		nil,
	},
	{
		APIRate{"mon,mon,mon", "0900-2100", "America/Chicago", 1500},
		&Rate{
			WeekdaySet{
				time.Monday: struct{}{},
			},
			900,
			2100,
			*chicago,
			1500,
		},
		nil,
	},
	{
		APIRate{"mon,tues,thurs", "0900-2100", "America/Chicago", 1500},
		&Rate{
			WeekdaySet{
				time.Monday:   struct{}{},
				time.Tuesday:  struct{}{},
				time.Thursday: struct{}{},
			},
			900,
			2100,
			*chicago,
			1500,
		},
		nil,
	},
	{
		APIRate{"fri,sat,sun", "0900-2100", "America/Chicago", 2000},
		&Rate{
			WeekdaySet{
				time.Friday:   struct{}{},
				time.Saturday: struct{}{},
				time.Sunday:   struct{}{},
			},
			900,
			2100,
			*chicago,
			2000,
		},
		nil,
	},
	{
		APIRate{"wed", "0600-1800", "America/Chicago", 1750},
		&Rate{
			WeekdaySet{
				time.Wednesday: struct{}{},
			},
			600,
			1800,
			*chicago,
			1750,
		},
		nil,
	},
	{
		APIRate{"mon,wed,sat", "0100-0500", "America/Chicago", 1000},
		&Rate{
			WeekdaySet{
				time.Monday:    struct{}{},
				time.Wednesday: struct{}{},
				time.Saturday:  struct{}{},
			},
			100,
			500,
			*chicago,
			1000,
		},
		nil,
	},
	{
		APIRate{"sun,tues", "0100-0700", "America/Chicago", 925},
		&Rate{
			WeekdaySet{
				time.Tuesday: struct{}{},
				time.Sunday:  struct{}{},
			},
			100,
			700,
			*chicago,
			925,
		},
		nil,
	},
	// bad time patterns
	{
		APIRate{"sun,tues", "100-0700", "America/Chicago", 925},
		nil,
		ErrTimePattern,
	},
	{
		APIRate{"sun,tues", "10000-0700", "America/Chicago", 925},
		nil,
		ErrTimePattern,
	},
	{
		APIRate{"sun,tues", "1000-100", "America/Chicago", 925},
		nil,
		ErrTimePattern,
	},
	{
		APIRate{"sun,tues", "1000-10000", "America/Chicago", 925},
		nil,
		ErrTimePattern,
	},
	{
		APIRate{"sun,tues", "-1000-0700", "America/Chicago", 925},
		nil,
		ErrTimePattern,
	},
	// time out of range
	{
		APIRate{"sun,tues", "1000-2400", "America/Chicago", 925},
		nil,
		ErrTimeOutOfRange,
	},
	{
		APIRate{"sun,tues", "2400-2359", "America/Chicago", 925},
		nil,
		ErrTimeOutOfRange,
	},
	// start time not before end
	{
		APIRate{"sun,tues", "0000-0000", "America/Chicago", 925},
		nil,
		ErrStartTimeNotBeforeEnd,
	},
	{
		APIRate{"sun,tues", "0001-0000", "America/Chicago", 925},
		nil,
		ErrStartTimeNotBeforeEnd,
	},
	{
		APIRate{"sun,tues", "2359-2358", "America/Chicago", 925},
		nil,
		ErrStartTimeNotBeforeEnd,
	},
	// bad timezone
	{
		APIRate{"mon,tues,thurs", "0900-2100", "america/chicago", 1500},
		nil,
		ErrTimezone,
	},
	{
		APIRate{"mon,tues,thurs", "0900-2100", "", 1500},
		nil,
		ErrTimezone,
	},
	// bad days of week
	{
		APIRate{"mon,tues,foo,wed", "0900-2100", "America/Chicago", 1500},
		nil,
		ErrBadDaysOfWeek,
	},
	{
		APIRate{"", "0900-2100", "America/Chicago", 1500},
		nil,
		ErrBadDaysOfWeek,
	},
	{
		APIRate{",mon,tues", "0900-2100", "America/Chicago", 1500},
		nil,
		ErrBadDaysOfWeek,
	},
	{
		APIRate{"mon,tues,", "0900-2100", "America/Chicago", 1500},
		nil,
		ErrBadDaysOfWeek,
	},
	{
		APIRate{"mon tues", "0900-2100", "America/Chicago", 1500},
		nil,
		ErrBadDaysOfWeek,
	},
}

// TestRateValidate tests various inputs against APIRate.AsRate
func TestRateValidate(t *testing.T) {
	for _, test := range validationTests {
		actual, errActual := test.in.AsRate()

		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("for %v: got %v, expected %v", test.in, actual, test.out)
		}

		if errActual != test.err {
			t.Errorf("for %v: got %v, expected %v", test.in, errActual, test.err)
		}
	}
}

var weekdaysFromStrTests = []struct {
	in  string
	out WeekdaySet
}{
	{
		"mon,tues,wed,thurs,fri,sat,sun",
		WeekdaySet{
			time.Monday:    struct{}{},
			time.Tuesday:   struct{}{},
			time.Wednesday: struct{}{},
			time.Thursday:  struct{}{},
			time.Friday:    struct{}{},
			time.Saturday:  struct{}{},
			time.Sunday:    struct{}{},
		},
	},
	{
		"foo,bar,baz",
		WeekdaySet{},
	},
	{
		"mon,foobar,mon,mon,fri",
		WeekdaySet{
			time.Monday: struct{}{},
			time.Friday: struct{}{},
		},
	},
	{
		",,,,mon,,,,,fri,tues,,,,",
		WeekdaySet{
			time.Monday:  struct{}{},
			time.Friday:  struct{}{},
			time.Tuesday: struct{}{},
		},
	},
	{
		"",
		WeekdaySet{},
	},
}

// TestWeekdaysFromStr tests various inputs against weekdaysFromStr
func TestWeekdaysFromStr(t *testing.T) {
	for _, test := range weekdaysFromStrTests {
		actual := weekdaysFromStr(test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("for %v: got %v, expected %v", test.in, actual, test.out)
		}
	}
}

var spansTimesTests = []struct {
	rate  *Rate
	start string
	end   string
	out   bool
}{
	// samples from code challenge
	{
		&Rate{
			WeekdaySet{
				time.Wednesday: struct{}{},
			},
			600,
			1800,
			*chicago,
			1750,
		},
		"2015-07-01T07:00:00-05:00",
		"2015-07-01T12:00:00-05:00",
		true,
	},
	{
		&Rate{
			WeekdaySet{
				time.Friday:   struct{}{},
				time.Saturday: struct{}{},
				time.Sunday:   struct{}{},
			},
			900,
			2100,
			*chicago,
			2000,
		},
		"2015-07-04T15:00:00+00:00",
		"2015-07-04T20:00:00+00:00",
		true,
	},
	{
		&Rate{
			WeekdaySet{
				time.Wednesday: struct{}{},
			},
			600,
			1800,
			*chicago,
			1750,
		},
		"2015-07-04T07:00:00+05:00",
		"2015-07-04T20:00:00+05:00",
		false,
	},

	// edge cases
	{
		&Rate{
			WeekdaySet{
				time.Wednesday: struct{}{},
			},
			601,
			1800,
			*chicago,
			1750,
		},
		"2015-07-01T06:00:00-05:00",
		"2015-07-01T12:00:00-05:00",
		false,
	},
	{
		&Rate{
			WeekdaySet{
				time.Wednesday: struct{}{},
			},
			600,
			1800,
			*chicago,
			1750,
		},
		"2015-07-01T06:00:00-05:00",
		"2015-07-01T12:00:00-05:00",
		true,
	},
	{
		&Rate{
			WeekdaySet{
				time.Wednesday: struct{}{},
			},
			600,
			1200,
			*chicago,
			1750,
		},
		"2015-07-01T06:00:00-05:00",
		"2015-07-01T12:00:00-05:00",
		false,
	},
	{
		&Rate{
			WeekdaySet{
				time.Wednesday: struct{}{},
			},
			600,
			1201,
			*chicago,
			1750,
		},
		"2015-07-01T06:00:00-05:00",
		"2015-07-01T12:00:00-05:00",
		true,
	},

	// wrong weekday
	{
		&Rate{
			WeekdaySet{
				time.Tuesday: struct{}{},
			},
			600,
			1800,
			*chicago,
			1750,
		},
		"2015-07-01T07:00:00-05:00",
		"2015-07-01T12:00:00-05:00",
		false,
	},
	{
		&Rate{
			WeekdaySet{
				time.Monday:    struct{}{},
				time.Tuesday:   struct{}{},
				time.Wednesday: struct{}{},
				time.Thursday:  struct{}{},
				time.Friday:    struct{}{},
				// time.Saturday:  struct{}{},
				time.Sunday: struct{}{},
			},
			900,
			2100,
			*chicago,
			2000,
		},
		"2015-07-04T15:00:00+00:00",
		"2015-07-04T20:00:00+00:00",
		false,
	},

	// crossed date boundary
	{
		&Rate{
			WeekdaySet{
				time.Wednesday: struct{}{},
			},
			2300,
			2359,
			*chicago,
			1750,
		},
		"2015-07-01T23:50:00-05:00",
		"2015-07-02T00:00:00-05:00",
		false,
	},
	{
		&Rate{
			WeekdaySet{
				time.Wednesday: struct{}{},
			},
			1200,
			2200,
			*chicago,
			1750,
		},
		"2015-07-01T10:00:00-06:00",
		"2015-07-01T23:00:00-06:00",
		false,
	},
}

// TestSpansTimes tests various inputs against Rates.SpansTimes
func TestSpansTimes(t *testing.T) {
	for _, test := range spansTimesTests {
		start, _ := time.Parse(time.RFC3339, test.start)
		end, _ := time.Parse(time.RFC3339, test.end)
		actual := test.rate.SpansTimes(start, end)

		if actual != test.out {
			t.Errorf(
				"for %v, %v, %v: got %v, expected %v",
				test.start,
				test.end,
				test.rate,
				actual,
				test.out,
			)
		}
	}

}
