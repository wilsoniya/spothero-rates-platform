package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// WeekdaySet is a set of days of the week
type WeekdaySet map[time.Weekday]struct{}

// ErrTimePattern indicates a failure to parse a time range
var ErrTimePattern = fmt.Errorf("times string doesn't match pattern")

// ErrTimezone indicates an unparseable time zone
var ErrTimezone = fmt.Errorf("unknown timezone given")

// ErrTimeOutOfRange indicates a time was encountered out of [0-2359]
var ErrTimeOutOfRange = fmt.Errorf("upper or lower time bound out of range [0-2359]")

// ErrStartTimeNotBeforeEnd indicates a start time not occurring strictly before the end time
var ErrStartTimeNotBeforeEnd = fmt.Errorf("start time not strictly before end time")

// ErrBadDaysOfWeek indicates a bad comma-delimited weekday string was given
var ErrBadDaysOfWeek = fmt.Errorf("bad days of week given")

// APIRates is an envelope type containing a collection of APIRate instances
type APIRates struct {
	APIRates []APIRate `json:"rates,omitempty"`
}

// APIRate is a representation of a parking rate applicable on a given set of
// days for a given time span. For use as an API payload. See Rate for general
// use.
type APIRate struct {
	// comma-delimited list of days of the week (in
	// [mon|tues|wed|thurs|fri|sat|sun] on which the rate is applicable.
	Days string `json:"days,omitempty"`

	// Times is a time range of rate validity of form HHMM-HHMM. The first time
	// must occur strictly before the second.
	Times string `json:"times,omitempty"`

	// TZ database name of timzone. See:
	// https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
	Tz string `json:"tz,omitempty"`

	// Price is the rate in USD cents
	Price uint32 `json:"price,omitempty"`
}

// AsRate validates this APIRate and transforms it into a Rate, or error.
func (r *APIRate) AsRate() (*Rate, error) {
	if len(r.Tz) == 0 {
		// prevent timezone loader from assuming UTC
		return nil, ErrTimezone
	}

	location, err := time.LoadLocation(r.Tz)
	if err != nil {
		return nil, ErrTimezone
	}

	pat := regexp.MustCompile(`^(\d{4})-(\d{4})$`)
	matches := pat.FindStringSubmatch(r.Times)
	if matches == nil {
		return nil, ErrTimePattern
	}

	begin, errBegin := strconv.ParseUint(matches[1], 10, 32)
	end, errEnd := strconv.ParseUint(matches[2], 10, 32)
	if errBegin != nil || errEnd != nil {
		return nil, ErrTimePattern
	}

	if begin > 2359 || end > 2359 {
		return nil, ErrTimeOutOfRange
	}

	if begin >= end {
		return nil, ErrStartTimeNotBeforeEnd
	}

	pat = regexp.MustCompile(`^(?:(?:mon|tues|wed|thurs|fri|sat|sun),)*(?:mon|tues|wed|thurs|fri|sat|sun)$`)
	if ok := pat.MatchString(r.Days); !ok {
		return nil, ErrBadDaysOfWeek
	}

	daysSet := weekdaysFromStr(r.Days)

	rate := Rate{
		DaysSet:        daysSet,
		BeginInclusive: uint16(begin),
		EndExclusive:   uint16(end),
		Tz:             *location,
		Price:          r.Price,
	}

	return &rate, nil
}

// Rate is a Representation of a rate applicable on a given set of days for a
// given time span.
type Rate struct {
	// DaysSet is the set of days of the week this Rate is valid
	DaysSet WeekdaySet

	// BeginInclusive is a time of the form HHMM at which this Rate begins
	BeginInclusive uint16

	// EndExclusive is a time of the form HHMM at which this Rate end
	EndExclusive uint16

	// Tz is the time zone by which the times of this Rate are contextualized
	Tz time.Location

	// Price is the price of this Rate in USD cents
	Price uint32
}

// SpansTimes determines whether this Rate encapsulates the time range
// beginQuery-endQuery
func (r *Rate) SpansTimes(beginQuery time.Time, endQuery time.Time) bool {
	beginQueryTzAdj := beginQuery.In(&r.Tz)
	endQueryTzAdj := endQuery.In(&r.Tz)

	if beginQueryTzAdj.Weekday() != endQueryTzAdj.Weekday() {
		// input range spans more than one day, which is invalid per
		// code challenge instructions:
		// "User input can span more than one day, but the API shouldn't return
		// a valid rate"
		Logger.Debugw(
			"Span invalid: input range spans more than one day",
			"start", beginQueryTzAdj.Weekday(),
			"end", endQueryTzAdj.Weekday(),
		)

		return false
	}

	if _, weekdayMatch := r.DaysSet[beginQueryTzAdj.Weekday()]; !weekdayMatch {
		Logger.Debugw(
			"Span invalid: Rate doesn't apply to the weekday in question",
			"weekday", beginQueryTzAdj.Weekday(),
			"rateWeekdays", r.DaysSet,
			"dayset", r.DaysSet,
		)
		return false
	}

	beginQueryMinSecs := beginQueryTzAdj.Hour()*100 + beginQueryTzAdj.Minute()
	endQueryMinSecs := endQueryTzAdj.Hour()*100 + endQueryTzAdj.Minute()

	if uint16(beginQueryMinSecs) < r.BeginInclusive {
		Logger.Debugw(
			"Span invalid: requested time range begins before this rate does",
			"requestBegin", beginQueryMinSecs,
			"rateBegin", r.BeginInclusive,
		)
		return false
	}

	if uint16(endQueryMinSecs) >= r.EndExclusive {
		Logger.Debugw(
			"Span invalid: requested time range ends at or after when this rate ends",
			"requestEnd", endQueryMinSecs,
			"rateEnd", r.EndExclusive,
		)
		return false
	}

	return true
}

// weekdayMap is a static map from day of the week abbreviation to time.Weekday
var weekdayMap = map[string]time.Weekday{
	"mon":   time.Monday,
	"tues":  time.Tuesday,
	"wed":   time.Wednesday,
	"thurs": time.Thursday,
	"fri":   time.Friday,
	"sat":   time.Saturday,
	"sun":   time.Sunday,
}

// weekdaysFromStr returns a set of Weekday enum variants present in
// commaDelimittedDays.
//
// This function requires commaDelimittedDays to be valid input. Valid input is
// comma-separated items from the set: {mon, tues, wed, thurs, fri, sat, sun}.
// Invalid weekdays are ignored.
func weekdaysFromStr(commaDelimittedDays string) WeekdaySet {
	daySet := WeekdaySet{}

	for _, day := range strings.Split(commaDelimittedDays, ",") {
		weekday, weekdayValid := weekdayMap[day]

		if !weekdayValid {
			// this should never happen
			Logger.Errorw(
				"Invalid weekday encountered and ignored during normalization",
				"weekday", day,
			)
			Statsd.Increment("rate.weekdays_from_str.invalid_day_of_week")

			continue
		}

		daySet[weekday] = struct{}{}
	}

	return daySet
}
