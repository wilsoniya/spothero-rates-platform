package main

import (
	"reflect"
	"testing"
	"time"
)

var allRates = []Rate{
	{
		WeekdaySet{
			time.Monday:   struct{}{},
			time.Tuesday:  struct{}{},
			time.Thursday: struct{}{},
		},
		900,
		2100,
		*chicago,
		1500,
	},
	{
		WeekdaySet{
			time.Friday:   struct{}{},
			time.Saturday: struct{}{},
			time.Sunday:   struct{}{},
		},
		900,
		2100,
		*chicago,
		2000,
	},
	{
		WeekdaySet{
			time.Wednesday: struct{}{},
		},
		600,
		1800,
		*chicago,
		1750,
	},
	{
		WeekdaySet{
			time.Monday:    struct{}{},
			time.Wednesday: struct{}{},
			time.Saturday:  struct{}{},
		},
		100,
		500,
		*chicago,
		1000,
	},
	{
		WeekdaySet{
			time.Tuesday: struct{}{},
			time.Sunday:  struct{}{},
		},
		100,
		700,
		*chicago,
		925,
	},
}

var getRatesTests = []struct {
	start string
	end   string
	out   []Rate
}{
	{
		"2015-07-01T07:00:00-05:00",
		"2015-07-01T12:00:00-05:00",
		[]Rate{
			{
				WeekdaySet{
					time.Wednesday: struct{}{},
				},
				600,
				1800,
				*chicago,
				1750,
			},
		},
	},
	{
		"2015-07-04T15:00:00+00:00",
		"2015-07-04T20:00:00+00:00",
		[]Rate{
			{
				WeekdaySet{
					time.Friday:   struct{}{},
					time.Saturday: struct{}{},
					time.Sunday:   struct{}{},
				},
				900,
				2100,
				*chicago,
				2000,
			},
		},
	},
	{
		"2015-07-04T07:00:00+05:00",
		"2015-07-04T20:00:00+05:00",
		[]Rate{},
	},
}

// TestGetRates tests various inputs against RatesStore.GetRates
func TestGetRates(t *testing.T) {
	store := GetStore()
	store.SetRates(allRates)

	for _, test := range getRatesTests {
		begin, _ := time.Parse(time.RFC3339, test.start)
		end, _ := time.Parse(time.RFC3339, test.end)

		actual := store.GetRates(begin, end)

		if !reflect.DeepEqual(test.out, actual) {
			t.Errorf("for %v -> %v, got %v, expected %v", begin, end, actual, test.out)
		}
	}
}

func TestSetRates(t *testing.T) {
	store := GetStore()
	memStore := store.(*inMemoryRatesStore)

	// first clear the store
	store.SetRates([]Rate{})
	if len(memStore.rates) != 0 {
		t.Fatalf("inMemoryRatesStore should be empty")
	}

	// now add some Rates and assert their presence
	store.SetRates(allRates)
	if len(memStore.rates) != 5 {
		t.Fatalf("inMemoryRatesStore should contain 5 rates")
	}

	// now clear the store again and assert its emptiness
	store.SetRates([]Rate{})
	if len(memStore.rates) != 0 {
		t.Fatalf("inMemoryRatesStore should be empty")
	}
}
