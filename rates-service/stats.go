package main

import (
	"sort"
	"sync"
	"time"
)

// inMemStoreIngressBufSize is the number of elements which may be held in the
// inMemoryStatsStore ingress buffer before subsequent puts block
const inMemStoreIngressBufSize uint16 = 1024

// RouteSummary encapsulates summary stats pertaining to a specific route for
// the duration of the rates-service process lifetime
type RouteSummary struct {
	// RouteName is a distinguishing name for the route
	RouteName string `json:"routeName"`

	// MeanElapsedMs is the average time elapsed while processing the route in
	// milliseconds
	MeanElapsedMs float32 `json:"meanElapsedMs"`

	// HitCount is the total number of invocations of the route
	HitCount uint64 `json:"hitCount"`
}

// Summary is a summarization of the preforamnce of API routes
type Summary struct {
	// RouteSummaries describe the performance of each route that has been
	// invoked
	RouteSummaries []RouteSummary `json:"routeSummaries"`

	// upTimeSecs is the total time in seconds since the rates-service process
	// was started
	UptimeSecs uint32 `json:"uptimeSecs"`
}

// StatsStore is a thing which stores performance statistics about API routes
type StatsStore interface {
	// TimeRoute initiates a timing of the route described by routeName. It
	// returns a function which, when invoked, concludes the timing and saves
	// the measured route statistic.
	TimeRoute(routeName string) func()

	// GetSummary computes a to-date summary of statistics for all route
	// measurements. Only routes which have been measured with TimeRoute are
	// included in the summary.
	GetSummary() Summary

	// Close terminates the StatsStore and indicates no more calls to
	// TimeRoute will follow.
	//
	// Warning: Calling TimeRoute after Close may cause a panic.
	Close()
}

// routeState encapsulates the to-date accumulation of measurements about a route
type routeState struct {
	elapsedMsAccum float32
	hitCount       uint64
}

// routeMeasurement is an individual data point of performance of a route
type routeMeasurement struct {
	RouteName string
	ElapsedMs float32
}

// inMemoryStatsStore is a StatsStore which stores statistics in memory.
// Stats are pushed asynchronously to optimize for speed of measurement.
type inMemoryStatsStore struct {
	startupTime          time.Time
	routeStats           map[string]routeState
	readMutex            sync.Mutex
	incomingMeasurements chan routeMeasurement
}

// NewStatsStore constructs and returns a concrete StatsStore.
func NewStatsStore() StatsStore {
	store := &inMemoryStatsStore{
		startupTime:          time.Now(),
		routeStats:           make(map[string]routeState),
		incomingMeasurements: make(chan routeMeasurement, inMemStoreIngressBufSize),
	}

	go store.run()

	return store
}

func (s *inMemoryStatsStore) TimeRoute(routeName string) func() {
	start := time.Now()

	return func() {
		elapsedMs := float32(time.Since(start).Seconds() * 1000)
		measurement := routeMeasurement{
			RouteName: routeName,
			ElapsedMs: elapsedMs,
		}
		s.incomingMeasurements <- measurement
	}
}

func (s *inMemoryStatsStore) GetSummary() Summary {
	s.readMutex.Lock()
	defer s.readMutex.Unlock()

	summaries := make([]RouteSummary, 0, len(s.routeStats))

	for name, state := range s.routeStats {
		summary := RouteSummary{
			RouteName:     name,
			MeanElapsedMs: state.elapsedMsAccum / float32(state.hitCount),
			HitCount:      state.hitCount,
		}

		summaries = append(summaries, summary)
	}

	sort.Slice(summaries, func(a, b int) bool {
		return summaries[a].RouteName < summaries[b].RouteName
	})

	return Summary{
		RouteSummaries: summaries,
		UptimeSecs:     uint32(time.Since(s.startupTime).Seconds()),
	}
}

func (s *inMemoryStatsStore) Close() {
	Logger.Infow("inMemoryStatsStore is shutting down")
	close(s.incomingMeasurements)
}

// run is the inMemoryStatsStore runtime loop which digests the incoming stream
// of measurements.
func (s *inMemoryStatsStore) run() {
	Logger.Infow("inMemoryStatsStore runtime loop started")

	for measurement := range s.incomingMeasurements {
		s.readMutex.Lock()

		state, stateExists := s.routeStats[measurement.RouteName]

		if !stateExists {
			state = routeState{}
		}

		state.hitCount++
		state.elapsedMsAccum += measurement.ElapsedMs
		s.routeStats[measurement.RouteName] = state

		s.readMutex.Unlock()
	}

	Logger.Infow("inMemoryStatsStore runtime loop exiting")
}
