package main

import (
	"testing"
	"time"
)

func summaryEquals(a, b Summary) bool {
	if len(a.RouteSummaries) != len(b.RouteSummaries) {
		return false
	}

	for i, ars := range a.RouteSummaries {
		brs := b.RouteSummaries[i]

		if !(ars.RouteName == brs.RouteName && ars.HitCount == brs.HitCount) {
			// intentionally ignore MeanElapsedMs
			return false
		}
	}

	// intentionally ignore UptimeSecs comparison
	return true
}

func TestStatsStore(t *testing.T) {
	store := NewStatsStore()

	actual := store.GetSummary()
	expected := Summary{
		// necessary for deep equality
		RouteSummaries: make([]RouteSummary, 0),
	}

	if !summaryEquals(actual, expected) {
		t.Errorf("Expected an empty summary: got %v, expected %v", actual, expected)
	}

	store.TimeRoute("route")()
	store.TimeRoute("route")()
	store.TimeRoute("route")()

	time.Sleep(time.Millisecond)

	actual = store.GetSummary()
	summaries := make([]RouteSummary, 0, 1)
	summaries = append(summaries, RouteSummary{"route", 0, 3})
	expected = Summary{
		RouteSummaries: summaries,
	}

	if !summaryEquals(actual, expected) {
		t.Logf("len(actual.RouteSummaries) = %v", len(actual.RouteSummaries))
		t.Errorf("Wrong summary: got %v, expected %v", actual, expected)
	}

	defer store.Close()
}
